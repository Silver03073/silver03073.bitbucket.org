$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	};
	$('body').removeClass('loaded'); 
});

$(document).ready(function() {    
    $('.slider .slider-wrapper').slick({
        arrows: false,
        dots: true,
        speed: 300,
        mobileFirst: true
    });            
    
    sliderArrowsControl();
    menuFadeIn();
    searchAnim();  
    
     if (window.matchMedia('(max-width: 320px)').matches) {
      $(".slider .slider-wrapper img").attr("src","img/slide__image-mobile.png");
   }
   else if (window.matchMedia('(max-width: 1920px)').matches) {
      $(".slider .slider-wrapper img").attr("src","img/slide__image.png");
   }  
    
}); // end ready

function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
$(function(){

	$('.button-nav').click(function(){
		$(this).toggleClass('active'), 
		$('.main-nav-list').slideToggle(); 
		return false;
	});
	

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();	
	$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
	
	
	var viewport_wid = viewport().width;
	
	if (viewport_wid <= 991) {
		
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);

function menuFadeIn() {
    $(".menu-left-catalog").hover(function(){
        $('.menu-left-dropdown').addClass('animated fadeIn');
    },
        function(){
        $('.menu-left-dropdown').removeClass('animated fadeIn');
    });
    
    $(".menu-left-dropdown > ul li:nth-child(4)").hover(function(){
        $('.menu-left-dropdown-side').addClass('animated fadeIn');
    },
        function(){
        $('.menu-left-dropdown-side').removeClass('animated fadeIn');
    });
    
}

function searchAnim() {
    $(".search input").focus(function(){
        $('.search img').addClass('animated pulse');
    });
    $(".search input").focusout(function(){
        $('.search img').removeClass('animated pulse');
    });
}

function sliderArrowsControl() {
    $(".slider__left-arrow").click(function(){
        $(".slider .slider-wrapper").slick('slickPrev');
    });
    $(".slider__right-arrow").click(function(){
        $(".slider .slider-wrapper").slick('slickNext');
    });
}
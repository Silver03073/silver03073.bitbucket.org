//jQuery is required to run this code
$( document ).ready(function() {

    $("input, select, textarea").jqBootstrapValidation();
    $("a[href*='#']").mPageScroll2id();
  
    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');

    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });
  
    $(".animsition").animsition({
  
    inClass               :   'fade-in-left',
    outClass              :   'fade-out-left',
    inDuration            :    1500,
    outDuration           :    800,
    linkElement           :   '.animsition-link',
    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
    loading               :    true,
    loadingParentElement  :   'body', //animsition wrapper element
    loadingClass          :   'animsition-loading',
    unSupportCss          : [ 'animation-duration',
                              '-webkit-animation-duration',
                              '-o-animation-duration'
                            ],
    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    
    overlay               :   false,
    
    overlayClass          :   'animsition-overlay-slide',
    overlayParentElement  :   'body'
  });
    
  $(".video-container .title_anim_1").animated("fadeInRight", "fadeOutRight");
  $("#courseId").animated("fadeInUp", "fadeOut");  
  $(".section-1 .title").animated("pulse", "fadeOut");
  
  $(".flag-icon").animated("pulse", "fadeOut");
  $("#start_study").animated("flipInY", "fadeOutY");
  $(".section-2 h1").animated("zoomIn", "zoomOut");
  $("#down_i, #down_i-1, #down_i-2, #down_i-3, #down_i-4, #down_i-5, #down_i-6").animated("bounceInDown", "fadeOut");
  $("#up_p, #up_p-2, #up_p-3,#up_p-4,#up_p-5,#up_p-6").animated("bounceInUp", "fadeOut");
  
  $(".section-3").animated("fadeIn", "fadeOut");
  
});

function scaleVideoContainer() {

    var height = $(window).height() + 5;
    var unitHeight = parseInt(height) + 'px';
    $('.homepage-hero-module').css('height',unitHeight);

}

function initBannerVideoSize(element){

    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
    windowHeight = $(window).height() + 5,
    videoWidth,
    videoHeight;

    console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width');

        $(this).width(windowWidth);

        if(windowWidth < 1000){
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

            $(this).width(videoWidth).height(videoHeight);
        }

        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');

    });
}
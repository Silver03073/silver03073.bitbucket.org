$(document).ready(function() 
{
    
    FancyBox();
	CarouselControl();
    AnimateProductSectionItems();    
    Scrolling();
    BackToTop();
    
    
});

$(window).load(function() {

	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow");

});

function FancyBox()
{
    //Попап менеджер FancyBox
	//Документация: http://fancybox.net/howto
	//<a class="fancybox"><img src="image.jpg" /></a>
	//<a class="fancybox" data-fancybox-group="group"><img src="image.jpg" /></a>
	$(".fancybox").fancybox();
}

function CarouselControl()
{
    
	//Каруселька
	//Документация: http://owlgraphic.com/owlcarousel/
	 //Carousel items height
    function CarouselHeightDetect() {
		$(".main_carousel div").css("height",               $(window).height()/2);        
        $(".item1, .item2, .item3, .item4").css("width", ($(window).width()));
        if($(window).width() >= 768) {
             $(".item1 button").css("top", $(".main_carousel div").height() - ($(window).height()/2.2));
        }               
	};
	CarouselHeightDetect();
	$(window).resize(function() {
		CarouselHeightDetect();
	});
    
    $("#owl_carousel").owlCarousel({
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true,
        transitionStyle : "fadeUp" ,
        pagination: false
    });
    
    var owl = $("#owl_carousel").data("owlCarousel");
    
      $('#goTo0').click(function(){
            owl.goTo(0);
    });
    
     $('#goTo1').click(function(){
            owl.goTo(1);
    });
    
      $('#goTo2').click(function(){
            owl.goTo(2);
           NumbersAnimation();
    });
    
      $('#goTo3').click(function(){
            owl.goTo(3);
            PercentsAnimation();
    });
}

function NumbersAnimation()
{
    $("#100").prop('number', 100).animateNumber({number: 100}, 1500);
    $("#150").prop('number', 100).animateNumber({number: 150}, 1600);
    $("#200").prop('number', 100).animateNumber({number: 200}, 1700);
    $("#300").prop('number', 100).animateNumber({number: 300}, 1800);
    $("#500").prop('number', 100).animateNumber({number: 500}, 1900);
    $("#800").prop('number', 100).animateNumber({number: 800}, 2000);
    $("#1000").prop('number', 1000).animateNumber({number: 1000}, 2100);
    
    
    $('#100').addClass('animated infinite pulse');
    $('#1000').addClass('animated infinite pulse');
}

function PercentsAnimation()
{
    $("#3per").animateNumber(
      {
        number: 3,
        numberStep: $.animateNumber.numberStepFactories.append(' %')
      },
      1100
    );
     $("#5per").animateNumber(
      {
        number: 5,
        numberStep: $.animateNumber.numberStepFactories.append(' %')
      },
      1500
    );
     $("#7per").animateNumber(
      {
        number: 7,
        numberStep: $.animateNumber.numberStepFactories.append(' %')
      },
      2000
    );
    $('#3per').addClass('animated infinite tada');
    $('#5per').addClass('animated infinite tada');
    $('#7per').addClass('animated infinite tada');
}

function AnimateProductSectionItems()
{
    $(".products_section p").animated("pulse", "fadeOut");  
    $(".products_section h2").animated("flipInX", "fadeOut");
    
    $("#i1, #i4, #i7, #i10, #i13, #i16, #i19, #i22").animated("fadeIn", "fadeOut");  
    $("#i2, #i5, #i8, #i11, #i14, #i17, #i20, #i23").animated("fadeIn", "fadeOut");  
    $("#i3, #i6, #i9, #i12, #i15, #i18, #i21, #i24").animated("fadeIn", "fadeOut");  
}

function Scrolling()
{
    $(".item1 a").mPageScroll2id();
    $(".to_top a").mPageScroll2id();
}

function BackToTop()
{
        var offset = 1000;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
              $(".up-btn").removeClass('animated slideOutRight');
             $(".up-btn").css("right", 10);
            $(".up-btn").css("display", "block");
            $(".up-btn").addClass('animated slideInRight');
        } else if(jQuery(this).scrollTop() < offset) {
              $(".up-btn").removeClass('animated slideInRight');
            $(".up-btn").css("right", 0);
             $(".up-btn").addClass('animated slideOutRight');
            //$(".up-btn").css("display", "none");                
        }
    });
}